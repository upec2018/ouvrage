package edu.upec.svl.soap;

import java.util.List;

import javax.ejb.EJB;
import javax.jws.WebService;

import edu.upec.m2.IOuvrageService;
import edu.upec.m2.model.Ouvrage;

@WebService
public class SoapOuvrageService{
	@EJB
	private IOuvrageService ejb;


	public Long createOuvrage(String titre, String auteur) {
		// TODO Auto-generated method stub
		return ejb.createOuvrage(titre, auteur);
	}


	public void deleteOuvrage(Long id) {
		ejb.deleteOuvrage(id);
		
	}


	public List<Ouvrage> getAllOuvrage() {
		// TODO Auto-generated method stub
		return ejb.getAllOuvrage();
	}


	public Ouvrage getOuvrageById(Long id) {
		// TODO Auto-generated method stub
		return ejb.getOuvrageById(id);
	}
	
	
	
}
