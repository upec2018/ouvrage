/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upec.m2.client;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author s4665982
 */
@ApplicationScoped
public class EntityManagerProducer {
    @Produces
    public EntityManager getEntityManager(){
        return Persistence.createEntityManagerFactory("myPU").createEntityManager();
    }
}
