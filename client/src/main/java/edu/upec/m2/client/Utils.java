package edu.upec.m2.client;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;
/**
 * 
 * @author Salifou BERTHE
 *
 */
public class Utils {
	private static final  String DATA_FORMAT = "dd/MM/yyyy"; 
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATA_FORMAT);
	
	public static final String dateToString(Date date) {
		LocalDate localDate = Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
		return localDate.format(formatter);
	}
    public static final String prompt(String question) {
        final Scanner sc = new Scanner(System.in);
        String input = "";

        do {
            System.out.println();
            System.out.println(question);
            input = sc.nextLine();
        } while (input == "");

        return input;
    }
    public static final long promptLong(String question, long min, long max) {
        final Scanner sc = new Scanner(System.in);
        long input = 0;

        do {
            System.out.println();
            System.out.println(question);

            if (sc.hasNextInt()) {
                input = sc.nextLong();
            }

        } while (input < min || input > max);

        return input;
    }        
}
