/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upec.m2.client;

import java.util.List;

/**
 *
 * @author s4665982
 */
public interface DaoInterface<P, E> {
    P persist(E e);
    void delete(E e);
    List<E> displayAll();
}
