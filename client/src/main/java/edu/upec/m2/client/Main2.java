/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upec.m2.client;

import edu.upec.m2.model.Ouvrage;
import edu.upec.m2.model.OuvrageStatusType;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.se.SeContainer;
import javax.enterprise.inject.se.SeContainerInitializer;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author s4665982
 */
public class Main2 {

    public static final void main(String... args){
        SeContainerInitializer initializer = SeContainerInitializer.newInstance();
        SeContainer container = initializer.initialize();
        Instance<ServiceLocator> serviceLocatorinstances = container.select(ServiceLocator.class);
        ServiceLocator servicelocator  =serviceLocatorinstances.get();
        for(;;){
            int read = menu();
            switch(read){
                case 0:
                    return;
                case 1:
                    creerOuvrage( servicelocator);
                    break;
                case 2:
                    afficherOuvrages(servicelocator);
                    break;
                case 3:
                	editerOuvrage(servicelocator);
                    break;
                case 4:
                	supprimerOuvrage(servicelocator);
                    break;
                default: 
                    break;
            }            
        }

    }
    /**
     * 
     * @return
     */
    public static final int menu(){
        System.out.println("0. Sortir du programme");
        System.out.println("1. Créer un ouvrage");
        System.out.println("2. Afficher les ouvrages");
        System.out.println("3. Editer un ouvrage");
        System.out.println("4. Supprimer un ouvrage");
        System.out.println("Entrez votre choix");
        Scanner sc = new Scanner(System.in);
        int read = -1;
        do{
            read = sc.nextInt();
        }while(read<0 || read>4);
        return read;
    }
    /**
     * 
     * @param servicelocator
     */
    public static void creerOuvrage(ServiceLocator servicelocator){
         Scanner sc = new Scanner(System.in);

         Long id = servicelocator.getOuvrageService().createOuvrage(Utils.prompt("Entrez le titre"), Utils.prompt("Entrez l'auteur"));
         System.out.println("L'ouvrage a été créé avec l'id : "+id);
    }
    /**
     * 
     * @param servicelocator
     */
    public static final void afficherOuvrages(ServiceLocator servicelocator){
    	List<Ouvrage> ouvrages  = servicelocator.getOuvrageService().getAllOuvrage();
    	if(ouvrages!=null) {
    		ouvrages.stream().forEach(o -> System.out.println("ID = "+o.getId()+" titre = "+o.getTitre()));
    	}
    }
    /**
     * 
     * @param servicelocator
     */
    public static void editerOuvrage(ServiceLocator servicelocator){
    	Ouvrage o = servicelocator.getOuvrageService().getOuvrageById(Utils.promptLong("Entrez l'id de l'ouvrage", Long.MIN_VALUE, Long.MAX_VALUE));
    	if(o!=null) {
    		System.out.println("ID : "+o.getId());
    		System.out.println("Auteur : "+o.getAuteur());
    		System.out.println("Description : "+o.getDescription());
    		System.out.println("Titre : "+o.getTitre());
    		System.out.println("Date de publication : "+Utils.dateToString(o.getDateDePublication()));
    		System.out.println("Status : "+o.getStatut());
    	}
    }
    /**
     * 
     * @param servicelocator
     */
    public static final void supprimerOuvrage(ServiceLocator servicelocator){
    	Ouvrage o = servicelocator.getOuvrageService().getOuvrageById(Utils.promptLong("Entrez l'id de l'ouvrage", Long.MIN_VALUE, Long.MAX_VALUE));
    	if(o!=null) {
    		servicelocator.getOuvrageService().deleteOuvrage(o.getId());
    	}else {
    		System.out.println("L'ouvrage "+o.getId()+" n'existe pas en base.");
    	}
    }    
}
