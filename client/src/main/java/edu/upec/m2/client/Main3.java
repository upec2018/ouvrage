package edu.upec.m2.client;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import edu.upec.m2.IMonEJB;
import edu.upec.m2.IOuvrageService;

public class Main3 {
 public static void main(String[] args) throws NamingException {
     Properties prop = new Properties();

     prop.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
     prop.put(Context.PROVIDER_URL, "http-remoting://localhost:8080");
     prop.put(Context.SECURITY_PRINCIPAL, "upec");
     prop.put(Context.SECURITY_CREDENTIALS, "upec");

     prop.put("jboss.naming.client.ejb.context", true);
     prop.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

     Context context =  new InitialContext(prop);	
     IOuvrageService service = (IOuvrageService) context.lookup("ejb:ear-1.0.0-SNAPSHOT/ejb-1.0.0-SNAPSHOT/OuvrageService!edu.upec.m2.IOuvrageService");
     System.out.println(service.createOuvrage("YYYY2", "AAAAA2"));
     service.getAllOuvrage().stream().forEach(o ->System.out.println(o.getAuteur()+"  "+o.getTitre()));
     
 }
}
