/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upec.m2.client;

import edu.upec.m2.model.Ouvrage;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author s4665982
 */
@ApplicationScoped 
public class OuvrageDaoimpl implements DaoInterface<Long, Ouvrage>{
    @Inject
    private EntityManager em;
    @Override
    public Long persist(Ouvrage o) {

        try{
            em.getTransaction().begin();
            em.persist(o);
            em.getTransaction().commit();
            return o.getId();
        }catch(Throwable t){
            em.getTransaction().rollback();
            return -1L;
        }finally{
            em.close();
        } 
        
    }

    @Override
    public void delete(Ouvrage e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Ouvrage> displayAll() {
                try{
            TypedQuery<Ouvrage> myquuery = em.createQuery("SELECT o FROM Ouvrage o", Ouvrage.class);
            List<Ouvrage> ouvrages = myquuery.getResultList();
            return ouvrages;
        }finally{
            em.close();
        }
    }
    
}
