package edu.upec.m2.client;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import edu.upec.m2.IOuvrageService;
/**
 * 
 * @author Salifou BERTHE
 *
 */
@ApplicationScoped
public class ServiceLocator {
	private static final String OUVRAGE_SERVICE_NAME = "ejb:ear-1.0.0-SNAPSHOT/ejb-1.0.0-SNAPSHOT/OuvrageService!edu.upec.m2.IOuvrageService";
	
    private Context context = null;
    private Map<String, Object> cache = Collections.synchronizedMap(new HashMap<String, Object>());
    /**
     * 
     */
    public ServiceLocator(){

    }	
    @PostConstruct
    public void init() {
        try {
            context = this.getJNDIContext("localhost", 8080, "upec", "upec");  //Mauvaise pratique de mettre ne dur les l'url et mots de passe
        } catch (NamingException ex) {
            //DO NOTHING
        }    	
    }
    @PreDestroy
    public void clear() {
    	if(context!=null)
			try {
				context.close();
			} catch (NamingException e) {
				//DO NOTHING
			}
    }
    /**
     * 
     * 
     * @param serverName
     * @param port
     * @param username
     * @param password
     * @return
     * @throws NamingException 
     */
    private Context getJNDIContext(String serverName, int port, String username, String password) throws NamingException {
        Properties prop = new Properties();

        prop.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
        prop.put(Context.PROVIDER_URL, "http-remoting://"+serverName+":"+port);
        prop.put(Context.SECURITY_PRINCIPAL, username);
        prop.put(Context.SECURITY_CREDENTIALS, password);

        prop.put("jboss.naming.client.ejb.context", true);
        prop.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

        return new InitialContext(prop);
    }

    /**
     * 
     * @return
     */
    public IOuvrageService getOuvrageService() {
        if (!this.cache.containsKey(OUVRAGE_SERVICE_NAME)){
            try {
            	IOuvrageService iservice = (IOuvrageService) this.context.lookup(OUVRAGE_SERVICE_NAME) ;
            	this.cache.put(OUVRAGE_SERVICE_NAME, iservice);
            } catch (NamingException ex) {
                throw new ServiceLocatorException("",ex);
            }        	
        }
        return (IOuvrageService) this.cache.get(OUVRAGE_SERVICE_NAME);    	
    }
    
}
