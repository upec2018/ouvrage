package edu.upec.m2.client;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import edu.upec.m2.IOuvrageService;

public class EJBClient {
  public static void main(String[] args) throws Exception {
	  Context context = getJNDIContext("localhost", 8080, "upec", "upec");
	  Long id=createOuvrage(context);
	  System.out.println("================"+id);
	  context.close();
	  //getContext().lookup("java:jboss/mail/gmail");
  }
  public static final Long createOuvrage(Context context ) throws NamingException {

      return ((IOuvrageService) context.lookup("ejb:ear-1.0.0-SNAPSHOT/ejb-1.0.0-SNAPSHOT/OuvrageService!edu.upec.m2.IOuvrageService")).createOuvrage("", "");	  
  }  
  public static final IOuvrageService getOuvrageService(Context context ) throws NamingException {

      return (IOuvrageService) context.lookup("ejb:ear-1.0.0-SNAPSHOT/ejb-1.0.0-SNAPSHOT/OuvrageService!edu.upec.m2.IOuvrageService");	  
  }
  private static final Context getJNDIContext(String serverName, int port, String username, String password) throws NamingException {
      Properties prop = new Properties();

      prop.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
      prop.put(Context.PROVIDER_URL, "http-remoting://"+serverName+":"+port);
      prop.put(Context.SECURITY_PRINCIPAL, username);
      prop.put(Context.SECURITY_CREDENTIALS, password);

      prop.put("jboss.naming.client.ejb.context", false);
      prop.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

      return new InitialContext(prop);
  }  
  public static final Context getContext() throws NamingException {
      Properties jndiProps = new Properties();
      //jndiProps.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory"); 
    jndiProps.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
    jndiProps.put(Context.PROVIDER_URL,"http-remoting://localhost:8080/");
    jndiProps.put(Context.SECURITY_PRINCIPAL, "upec");
    jndiProps.put(Context.SECURITY_CREDENTIALS, "upec");
    jndiProps.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");  
    jndiProps.put("remote.connections", "default");  
    jndiProps.put("remote.connection.default.port", "8080");  
    jndiProps.put("remote.connection.default.host", "localhost");    
    //jndiProps.put("jboss.naming.client.ejb.context", true);
    jndiProps.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS", "false");  
    jndiProps.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
    return new InitialContext(jndiProps);	  
  }
}
