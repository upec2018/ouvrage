/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upec.m2.client;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author s4665982
 */
public class SendMail {
    private final String username = "upec.m2.java@gmail.com";
    private final String password = "Upec1010";    
    public void send(List<String> to, String title, String htmlMessage)throws MessagingException{
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587"); 
        
        Session session = Session.getInstance(props,
          new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                }
          });

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("upec.m2.java@gmail.com"));
        if(to!=null){
             message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(String.join(",", to))); 
        }

        message.setSubject(title);
        message.setText(htmlMessage);

        Transport.send(message);
      
    }
public static void main(String[] args) {

        try {
            new SendMail().send(Arrays.asList(""), "TITRE", "My message");
        } catch (MessagingException ex) {
            Logger.getLogger(SendMail.class.getName()).log(Level.SEVERE, null, ex);
        }
        



	}    
}
