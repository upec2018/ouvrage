/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upec.m2.client;



import javax.enterprise.inject.Instance;
import javax.enterprise.inject.se.SeContainer;
import javax.enterprise.inject.se.SeContainerInitializer;

import edu.upec.m2.IOuvrageService;

/**
 *
 * @author s4665982
 */
public class Main {
    public static final void main(String... args){
        SeContainerInitializer initializer = SeContainerInitializer.newInstance();
        SeContainer container = initializer.initialize();
        Instance<ServiceLocator> serviceLocatorinstances = container.select(ServiceLocator.class);    	
    	ServiceLocator servicelocator  =serviceLocatorinstances.get();
    	IOuvrageService service = servicelocator.getOuvrageService();
    	System.out.println("====>"+service.getAllOuvrage());
    	container.close();
        
    }
}
