package edu.upec.m2;

import javax.ejb.Remote;

@Remote
public interface IMonEJB {
  int getNumero();
}
