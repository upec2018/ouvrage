package edu.upec.m2;

public class OuvrageException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OuvrageException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OuvrageException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public OuvrageException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public OuvrageException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public OuvrageException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
