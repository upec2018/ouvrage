package edu.upec.m2;

import java.util.Timer;

import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateful;
import javax.ejb.Stateless;

@Singleton
@Startup
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class MonEJB implements IMonEJB {
     
	private static int COMPTEUR=0;
	
	private int numero;
	@PostConstruct
	public void init() {
		System.out.println("=========START MonEJB========");
		numero = ++COMPTEUR;
	}
	@Lock(LockType.WRITE)
	@Override
	public int getNumero() {
		try {
			Thread.currentThread().sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return numero;
	}

}
