package edu.upec.m2;

import java.util.List;

import javax.ejb.Remote;

import edu.upec.m2.model.Ouvrage;

@Remote
public interface IOuvrageService {
	Long createOuvrage(String titre, String auteur);
	void deleteOuvrage(Long id);
	List<Ouvrage> getAllOuvrage();
	Ouvrage getOuvrageById(Long id);
}
