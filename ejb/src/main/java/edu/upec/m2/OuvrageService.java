package edu.upec.m2;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import edu.upec.m2.model.Ouvrage;
import edu.upec.m2.model.OuvrageStatusType;
/**
 * 
 * @author Salifou BERTHE
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class OuvrageService implements IOuvrageService{
	private static Logger log = Logger.getLogger(OuvrageService.class.getName());
	@PersistenceContext
	private EntityManager em;
	
	
	@Resource(name="java:jboss/mail/gmail")
	private Session mailSession;
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Long createOuvrage2(String titre, String auteur) {
		Ouvrage o = new Ouvrage();
		o.setAuteur(auteur);
		o.setDateDePublication(new Date());
		o.setDescription("Une description");
		o.setStatut(OuvrageStatusType.DISPONIBLE);
		o.setTitre(titre);
		em.persist(o);
		return o.getId();
		//sendMessageMessage("salifou.berthe@gmail.com", "Nouvel ouvrage", "Création Ok pour "+o.getId());
		
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long createOuvrage(String titre, String auteur) {
		Ouvrage o = new Ouvrage();
		o.setAuteur(auteur+"AUTRE");
		o.setDateDePublication(new Date());
		o.setDescription("Une description");
		o.setStatut(OuvrageStatusType.DISPONIBLE);
		o.setTitre(titre+"AUTRE");
		em.persist(o);		
		return createOuvrage2( titre,  auteur);
	}
	public void sendMessageMessage(String to, String subject, String body) {
		
		try {
			Message message = new MimeMessage(mailSession);
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setText(body);
			
			Transport.send(message);
		} catch (MessagingException e) {
			log.log(Level.SEVERE, "Le message ne peut pas être envoyé", e);
			throw new OuvrageException(e);
		}
		
		
	}
	@Override
	public void deleteOuvrage(Long id) {
		em.remove(em.find(Ouvrage.class, id));
		
	}

	@Override
	public List<Ouvrage> getAllOuvrage() {
		return em.createNamedQuery("findAllOuvrages", Ouvrage.class).getResultList();
	}
	@Override
	public Ouvrage getOuvrageById(Long id) {
		TypedQuery<Ouvrage> typeQ = em.createNamedQuery("findOuvrageById", Ouvrage.class);
		typeQ.setParameter("nomparam", id);
		return typeQ.getSingleResult();
	}

}
