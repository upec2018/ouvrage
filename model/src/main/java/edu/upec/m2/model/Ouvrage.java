/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upec.m2.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author Salifou BERTHE
 */
@Entity
@SequenceGenerator(name = "maSeq", initialValue = 1, allocationSize = 50)

@NamedQuery(name = "findAllOuvrages", query = "SELECT o FROM Ouvrage o")
@NamedQuery(name = "findOuvrageById", query = "SELECT o FROM Ouvrage o WHERE o.id= :nomparam")
@NamedQuery(name = "findOuvragesByStatus", query = "SELECT o FROM Ouvrage o WHERE o.statut = :status")
@NamedQuery(name = "findOuvragesBeforeDate", query = "SELECT o FROM Ouvrage o WHERE o.dateDePublication < :dateDePublication")
@NamedQuery(name = "findOuvragesByReferenceAndStatus", query = "SELECT o FROM Ouvrage o WHERE o.reference= :reference AND o.statut= :statut")
public class Ouvrage implements Serializable{
    @Id
    @GeneratedValue(generator = "maSeq",strategy = GenerationType.SEQUENCE)
   private Long id;
   
    @Transient
    private transient String auteur;
    
    @Column(name = "TITLE", nullable = false, length = 256)
    private String titre;
    @Column(length = 3072, name = "DESCRIPTION", nullable = false)
    private String description; 
    @Column(name = "PUB_DATE", nullable = true) 
    @Temporal(TemporalType.DATE)
    private Date dateDePublication;
    
    @Enumerated(EnumType.STRING)
    private OuvrageStatusType statut;
    
    private String reference;
    
    @Lob
    private byte[] image;   

     public Long getId() {
         return id;
     }

     public void setId(Long id) {
         this.id = id;
     }

     public String getAuteur() {
         return auteur;
     }

     public void setAuteur(String auteur) {
         this.auteur = auteur;
     }

     public String getTitre() {
         return titre;
     }

     public void setTitre(String titre) {
         this.titre = titre;
     }

     public String getDescription() {
         return description;
     }

     public void setDescription(String description) {
         this.description = description;
     }

     public Date getDateDePublication() {
         return dateDePublication;
     }

     public void setDateDePublication(Date dateDePublication) {
         this.dateDePublication = dateDePublication;
     }

     public OuvrageStatusType getStatut() {
         return statut;
     }

     public void setStatut(OuvrageStatusType statut) {
         this.statut = statut;
     }

     public byte[] getImage() {
         return image;
     }

     public void setImage(byte[] image) {
         this.image = image;
     }

     public String getReference() {
         return reference;
     }

     public void setReference(String reference) {
         this.reference = reference;
     }
   
   
}
